import express, { Express, Request, Response, Router, NextFunction } from 'express';

export default (req: express.Request, res: express.Response, next: NextFunction) => {

    const jwt = req.app.get('jwt');
    const token = req.headers['x-access-token'] || req.query.token || req.body.token
    // decode token
    if (token) {
        // verifies secret and checks exp
        jwt.verify(token, process.env.CUSTOMER_SECRET_KEY, function (err: any, decoded : any) {
            if (err) {
                jwt.verify(token, process.env.EMPLOYEE_SECRET_KEY, function (err: any, decoded : any) {
                    if (err) {
                        return res.status(401).json({ "error": true, "message": 'Unauthorized access.' });
                    }
                    req.body.user = decoded;
                    next();
                })     
                return;
            }
            console.log(req.body);  

            req.body.user = decoded;
            next();
        });
    } else {
        // if there is no token
        // return an error
        return res.status(403).send({
            "error": true,
            "message": 'No token provided.'
        });
    }
}